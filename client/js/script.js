/*jshint esversion: 6 */
$(document).ready(function () {
    var socket = io('http://202.9.89.119:7000');
    var currentUser;
    function getUser() {
        $.get('/getuser', function (data) {
            var markerGroup = L.layerGroup().addTo(map);
            currentUser = data.user;
            $('#userName').html(data.user);
            $('#userLevel').html(data.lv);
            $('#userAtk').html(data.atk);
            $('#userDef').html(data.def);
            $('#userPoint').html(data.point);
            if (data.point > 0) {
                $('.statBtn').attr('disabled', false);
            } else {
                $('.statBtn').attr('disabled', true);
              }
            socket.emit('createplayer', {
                user: data.user,
                location: userlatlng
            });
            enemyPopup = [];
            socket.on('createplayer', function (player) {
                markerGroup.clearLayers();
                // console.log(player);
                // console.log(player.length);
                for (var i = 0; i < player.length; i++) {
                    // console.log('test');
                    // console.log(player[i].location);
                    if (player[i].user == data.user) {
                        L.marker(player[i].location, { icon: userIcon }).addTo(markerGroup);
                    } else {
                        console.log(player[i]);
                        enemyPopup[i] = L.popup({
                            closeButton: false,
                            autoClose: false
                        })
                        
                            .setLatLng([player[i].location.lat, player[i].location.lng ])
                            .setContent('\
                                <span class="font-weight-bold">Name:&nbsp;</span><span>'+player[i].user+'</span><br>\
                                <span class="font-weight-bold">Lv:&nbsp;</span><span>'+data.lv+'</span><br>\
                                <button class= "btn btn-danger btn-sm btn-block atkBtn" id="' + player[i].user + '">Attack</button>\
                                ');
                                // onclick="attack(\'' + player[i].user + '\')"
                        L.marker([player[i].location.lat, player[i].location.lng], { icon: enemyIcon }).addTo(markerGroup)
                        .bindPopup(enemyPopup[i]);
                    }
                }
            });
        });
    }
    // attack('test');
    
    
    function attack(enemy) {
        $.get('/battle?enemie='+enemy, function (data) {
            // alert(data);
            if (data == 'win') {
                $('.alert.alert-success').fadeIn(200);
            } else {
                $('.alert.alert-danger').fadeIn(200);
            }
            setTimeout( function () {
                // $(resultAlert).fadeOut();
                $('.alert.alert-success').fadeOut(200);
                $('.alert.alert-danger').fadeOut(200);
            }, '1000');
            
            $.get('/getuser', function (data) {
              currentUser = data.user;
              $('#userName').html(data.user);
              $('#userLevel').html(data.lv);
              $('#userAtk').html(data.atk);
              $('#userDef').html(data.def);
              $('#userPoint').html(data.point);
              if (data.point > 0) {
                $('.statBtn').attr('disabled', false);
              }
            });
  
            $.get('/gethistory', function (his) {
              $('#historyList').empty();
              if (his.length == '0') {
                  $('#historyList').append('<li class="list-group-item py-1 px-2">&nbsp;</li>');
              } else {
                  var i;
                  for (i = 0; i < 5; i++) {
                      $('#historyList').append('<li class="list-group-item py-1 px-2"><span class="font-weight-bold">' + his[i].enemie + '&nbsp:&nbsp;</span>' + his[i].result + '</li>');
                  }
              }
            });
        });
    }

    var msg;
    function chatMethod() {
        msg = $('#chatInput').val();
        socket.emit('chat', {
            user: currentUser,
            msg: msg
        });
        $('#chatInput').val('');
    }

    socket.on('chat', function (data) {
        L.popup({
            closeButton: false,
            autoClose: false
        })
            .setLatLng([data.location.lat, data.location.lng ])
            .setContent(data.msg)
            .openOn(map);
        closePopup();
    });

    function closePopup() {
        setTimeout(() => {
            $('#map').click();
        }, 2000);
    }

    $('#chatBtn').on('click', chatMethod);
    

    $.get('/gethistory', function (data) {
        // console.log(data);
        $('#historyList').empty();
        if (data.length == '0') {
            $('#historyList').append('<li class="list-group-item py-1 px-2">&nbsp;</li>');
        } else {
            var i;
            for (i = 0; i < 5; i++) {
                $('#historyList').append('<li class="list-group-item py-1 px-2"><span class="font-weight-bold">' + data[i].enemie + '&nbsp:&nbsp;</span>' + data[i].result + '</li>');
            }
        }
    });

    // gameplay.html

    
    // Leaflet code go here
    var map = L.map('map').fitWorld();
    var user;
    var userlatlng;
    var userIcon = L.icon({
        iconUrl: '../images/user_icon.png',
        iconSize: [40.3, 41.6],
        iconAnchor: [20.15, 41],
        popupAnchor: [-1.5, -40]
    });
    var enemyIcon = L.icon({
        iconUrl: '../images/enemy.png',
        iconSize: [40.3, 41.6],
        iconAnchor: [20.15, 41],
        popupAnchor: [-1.5, -40]
    });
    
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);

    function onLocationFound(e) {
        var radius = e.accuracy / 2;

        userlatlng = e.latlng;
        // console.log('e:', e);
        // console.log('Current location:', e.latlng);
    
    }

    function onLocationError(e) {
        alert(e.message);
    }

    // map.on('locationfound', onLocationFound);
    map.on('locationfound', function (e) {
        onLocationFound(e);
        getUser();
        // console.log(e);
    });
    map.on('locationerror', onLocationError);

    map.locate({ setView: true, maxZoom: 16 });

    function atkUp() {
        $.get('/upatk', function (res) {
            getUser();
        });
    }
    
    function defUp() {
        $.get('/updef', function (res) {
            getUser();
        });
    }
    $(document).on('click', '#atkStatBtn', atkUp);
    $(document).on('click', '#defStatBtn', defUp);
    $(document).on('click', '.atkBtn', function () {
        var id = $(this).attr('id');
        attack(id);
    });

    function myfn() {
        alert('quit');
    }

    $(window).bind('beforeunload', function(){
        myfn();
        // return 'Are you sure you want to leave?';
    });
});


