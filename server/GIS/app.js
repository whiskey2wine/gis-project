/**
 * Node.js Login Boilerplate
 * More Info : http://kitchen.braitsch.io/building-a-login-system-in-node-js-and-mongodb/
 * Copyright (c) 2013-2016 Stephen Braitsch
 **/
var http = require('http');
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
var fs = require('fs');

var app = express();

app.locals.pretty = true;
app.set('port', process.env.PORT || 2053);
app.set('views', __dirname + '/app/server/views');
app.set('view engine', 'jade');
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(require('stylus').middleware({
	src: __dirname + '/app/public'
}));
app.use(express.static(__dirname + '/app/public'));

// build mongo database connection url //

var dbHost = process.env.DB_HOST || 'localhost'
var dbPort = process.env.DB_PORT || 27017;
var dbName = process.env.DB_NAME || 'GIS';

// mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
var dbURL = 'mongodb://' + dbHost + ':' + dbPort + '/' + dbName;
if (app.get('env') == 'live') {
	// prepend url with authentication credentials // 
	dbURL = 'mongodb://' + process.env.DB_USER + ':' + process.env.DB_PASS + '@' + dbHost + ':' + dbPort + '/' + dbName;
}

app.use(session({
	secret: 'faeb4453e5d14fe6f6d04637f78077c76c73d1b4',
	proxy: true,
	resave: true,
	saveUninitialized: true,
	store: new MongoStore({
		url: dbURL
	})
}));

require('./app/server/routes')(app);

var httpServer = http.createServer(app);
var io = require('socket.io')();
var player = [];
io.on('connection', function (socket) {
	socket.on('createplayer', function (data) {

		var check = false;
		if (player.length == 0) {
			player.push(data);
			io.emit('createplayer', player);
		} else {
			for (var i = 0; i < player.length; i++) {
				if (player[i].user == data.user) {
					check = true;
					break;
				}
			}
			if (check == false) {
				player.push(data);
				io.emit('createplayer', player);
			} else {
				console.log('เจอตัวซ้ำ');
				var i = findIndexByKeyValue(player, 'user', data.user);
				console.log(i);
				//				delete player[i];
				player.splice(i, 1);
				player.push(data);
				io.emit('createplayer', player);
			}
		}
		console.log(player);
		//io.emit('createplayer', data);


	});
	console.log('connection');
	socket.on('chat', function (data) {
		for (var i = 0; i < player.length; i++) {
			if (data.user == player[i].user) {
				data.location = player[i].location;
				io.emit('chat', data);
			}
		}

	});
});

function findIndexByKeyValue(obj, key, value) {
	for (var i = 0; i < obj.length; i++) {
		if (obj[i][key] == value) {
			return i;
		}
	}
	return null;
}

io.listen(7000);
httpServer.listen(5000);
