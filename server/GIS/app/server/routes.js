var AM = require('./modules/account-manager');

module.exports = function (app) {

	app.get('/test', function (req, res) {
		res.send('test');
	});



	app.post('/', function (req, res) {
		AM.manualLogin(req.body['user'].toLowerCase(), req.body['pass'], function (e, o) {
			if (!o) {
				res.status(400).send(e);
				//				console.log(e);
			} else {
				req.session.user = o;
				if (req.body['remember-me'] == 'true') {
					res.cookie('user', o.user, {
						maxAge: 900000
					});
					res.cookie('pass', o.pass, {
						maxAge: 900000
					});
				}
				res.status(200).send(o);
				//				console.log(o);
			}
		});
	});


	// logged-in user homepage //
	app.get('/home', function (req, res) {
		if (req.session.user == null) {
			// if user is not logged-in redirect back to login page //
			res.redirect('/');
		} else {
			console.log('test');
		}
	});



	app.get('/logout', function (req, res) {
		res.clearCookie('user');
		res.clearCookie('pass');
		req.session.destroy(function (e) {
			res.redirect('/');
		});
	})

	app.post('/signup', function (req, res) {
		console.log('test');
		console.log(req.body);
		AM.addNewAccount({
			user: req.body['user'].toLowerCase(),
			pass: req.body['pass'],
		}, function (e) {
			if (e) {
				res.status(400).send(e);
			} else {
				res.status(200).send('ok')
			}
		});
	});

	app.get('/getuser', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.getUser(req.session.user.user, function (user) {
				res.status(200).send(user);
			});
		}
	});

	app.get('/upatk', function k(req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.upatk(req.session.user.user, function (user) {
				res.status(200).send('ok');
			});
		}
	});
	app.get('/updef', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.updef(req.session.user.user, function (user) {
				res.status(200).send('ok');
			});
		}
	});

	app.get('/gethistory', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.getHistory(req.session.user.user, function (history) {
				res.status(200).send(history);
			});
		}
	});

	app.get('/getscoreboard', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.getAlluser(function (alluser) {
				res.status(200).send(alluser);
			});
		}


	});

	app.get('/equip', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.equip(req.session.user.user, req.query.item, function (response) {
				if (response) {
					res.status(200).send('complete');
				}
			})
		}


	});

	app.get('/unequip', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.unequip(req.session.user.user, req.query.item, function (response) {
				if (response) {
					res.status(200).send('complete');
				}
			})
		}


	});

	app.get('/battle', function (req, res) {
		if (req.session.user == null) {
			res.redirect('/');
		} else {
			AM.battle(req.session.user.user, req.query.enemie, function (result) {
				res.status(200).send(result);
			});

		}


	});


	app.get('*', function (req, res) {

	});



};
