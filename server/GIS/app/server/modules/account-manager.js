var crypto = require('crypto');
var MongoDB = require('mongodb').Db;
var Server = require('mongodb').Server;
var moment = require('moment');
//var randomstring = require("randomstring");
/*
	ESTABLISH DATABASE CONNECTION
*/
var dbName = process.env.DB_NAME || 'GIS';
var dbHost = process.env.DB_HOST || 'localhost'
var dbPort = process.env.DB_PORT || 27017;
var db = new MongoDB(dbName, new Server(dbHost, dbPort, {
	auto_reconnect: true
}), {
	w: 1
});
db.open(function (e, d) {
	if (e) {
		console.log(e);
	} else {
		if (process.env.NODE_ENV == 'live') {
			db.authenticate(process.env.DB_USER, process.env.DB_PASS, function (e, res) {
				if (e) {
					console.log('mongo :: error: not authenticated', e);
				} else {
					console.log('mongo :: authenticated and connected to database :: "' + dbName + '"');
				}
			});
		} else {
			console.log('mongo :: connected to database :: "' + dbName + '"');
		}
	}
});
var accounts = db.collection('accounts');
var history = db.collection('history');
var item = db.collection('item')


/* login validation methods */
exports.autoLogin = function (user, pass, callback) {
	accounts.findOne({
		user: user
	}, function (e, o) {
		if (o) {
			o.pass == pass ? callback(o) : callback(null);
		} else {
			callback(null);
		}
	});
}
exports.manualLogin = function (user, pass, callback) {
	accounts.findOne({
		user: user
	}, function (e, o) {
		if (o == null) {
			callback('user-not-found');
		} else {
			validatePassword(pass, o.pass, function (err, res) {
				if (res) {
					callback(null, o);
					//					console.log(o);
				} else {
					callback('invalid-password');
				}
			});
		}
	});
}



/* record insertion, update & deletion methods */
exports.addNewAccount = function (newData, callback) {
	accounts.findOne({
		user: newData.user
	}, function (e, o) {
		if (o) {
			callback('username-taken');
		} else {
			newData.lv = 1;
			newData.energy = 5;
			newData.atk = 1;
			newData.def = 1;
			newData.win = 0;
			newData.lose = 0;
			newData.point = 0;
			newData.equipped = {
				head: 'none',
				body: 'none',
				weapon: 'none'
			};
			newData.inventory = [{
				name: 'ดาบไม้'
			}];
			saltAndHash(newData.pass, function (hash) {
				newData.pass = hash;
				accounts.insert(newData, {
					safe: true
				}, callback);
			});
		}
	});
}


exports.getUser = function (user, callback) {
	accounts.findOne({
		user: user
	}, function (e, o) {
		if (o) {
			callback(o);
		}
	});
}

exports.getAlluser = function (callback) {
	accounts.find({}).sort({
		_id: 1
	}).toArray(function (e, o) {
		callback(o);

	});


}


exports.getHistory = function (user, callback) {
	history.find({
		user: user
	}).sort({
		_id: 1
	}).toArray(function (e, o) {
		callback(o);
	});
}

exports.getItem = function (itemname, callback) {
	item.find({
		name: itemname
	}).sort({
		_id: 1
	}).toArray(function (e, o) {
		callback(o);
	});
}

exports.equip = function (user, item, callback) {
	item.findOne({
		name: item
	}, function (e, o) {
		if (o) {
			accounts.findOne({
				user: user
			}, function (e, player) {
				if (player) {
					if (player.equipped[o.type] == 'none') {
						var tempequipped = {};
						tempequipped[o.type] = o.name;
						var tempATK = player.atk + o.atk;
						var tempDEF = player.def + o.def;
						accounts.update({
							user: user
						}, {
							$set: {
								atk: tempATK,
								def: tempDEF,
								equipped: tempequipped
							}
						}, function (e, o) {
							callback('done');
						});

					} else {
						item.findOne({
							name: player.equipped[o.type]
						}, function (e, olditem) {
							if (olditem) {


								var tempequipped = {};
								tempequipped[olditem.type] = 'none';
								var tempATK = player.atk - olditem.atk;
								var tempDEF = player.def - olditem.def;
								accounts.update({
									user: user
								}, {
									$set: {
										atk: tempATK,
										def: tempDEF,
										equipped: tempequipped
									}
								}, function (e, o) {
									callback('done');
								});
							}
						});

					}


				}
			});


		}
	});

}

exports.unequip = function (user, item, callback) {

	item.findOne({
		name: item
	}, function (e, o) {
		if (o) {
			accounts.findOne({
				user: user
			}, function (e, player) {
				if (player) {

					var tempequipped = {};
					tempequipped[o.type] = 'none';
					var tempATK = player.atk - o.atk;
					var tempDEF = player.def - o.def;
					accounts.update({
						user: user
					}, {
						$set: {
							atk: tempATK,
							def: tempDEF,
							equipped: tempequipped
						}
					}, function (e, o) {
						callback('done');
					});
				}
			});


		}
	});

}

exports.battle = function (user, enemie, callback) {
	accounts.findOne({
		user: user
	}, function (e, player) {
		if (player) {
			accounts.findOne({
				user: enemie
			}, function (e, figther) {
				if (figther) {
					var result = player.atk - figther.def;
					var randomnumber = randomIntFromInterval(0, 100);
					var playerwinrate = 50 + (result);
					if (randomnumber <= playerwinrate) {
						//playerwin
						var playerwin = player.win + 1;
						var playerlv = player.lv + 1;
						var playerpoint = player.point + 1;
						accounts.update({
							user: user
						}, {
							$set: {
								win: playerwin,
								point: playerpoint,
								lv: playerlv
							}
						}, function (e, o) {
							history.insert({
								user: user,
								result: 'win',
								enemie: figther.user
							});
							callback('win');
						});

					} else {
						//playerlose
						var playerlose = player.lose + 1;
						accounts.update({
							user: user
						}, {
							$set: {
								lose: playerlose,
							}
						}, function (e, o) {
							history.insert({
								user: user,
								result: 'lose',
								enemie: figther.user
							});
							callback('lose');
						});
					}


				}
			});
		}
	});

}

function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}



exports.upatk = function (user, callback) {
	accounts.findOne({
		user: user
	}, function (e, o) {
		if (o) {
			if (o.point != 0) {
				var pointleft = o.point - 1;
				var atk = o.atk + 1;
				accounts.update({
					user: user
				}, {
					$set: {
						atk: atk,
						point: pointleft
					}
				}, function (e, o) {
					callback('done');
				});
			}
		}
	});


}

exports.updef = function (user, callback) {
	accounts.findOne({
		user: user
	}, function (e, o) {
		if (o) {
			if (o.point != 0) {
				var pointleft = o.point - 1;
				var def = o.def + 1;
				accounts.update({
					user: user
				}, {
					$set: {
						def: def,
						point: pointleft
					}
				}, function (e, o) {
					callback('done');
				});
			}
		}
	});


}

exports.updatePassword = function (email, newPass, callback) {
	accounts.findOne({
		email: email
	}, function (e, o) {
		if (e) {
			callback(e, null);
		} else {
			saltAndHash(newPass, function (hash) {
				o.pass = hash;
				accounts.save(o, {
					safe: true
				}, callback);
			});
		}
	});
}
/* account lookup methods */
exports.deleteAccount = function (id, callback) {
	accounts.remove({
		_id: getObjectId(id)
	}, callback);
}


exports.delete = function (database, id, callback) {
	db.collection(database).remove({
		_id: getObjectId(id)
	}, callback);
}


exports.getAccountByEmail = function (email, callback) {
	accounts.findOne({
		email: email
	}, function (e, o) {
		callback(o);
	});
}
exports.validateResetLink = function (email, passHash, callback) {
	accounts.find({
		$and: [{
			email: email,
			pass: passHash
		}]
	}, function (e, o) {
		callback(o ? 'ok' : null);
	});
}

exports.getAllRecords = function (codeset, typeset, callback) {
	if (typeset.length == 0 && codeset.length > 0) {
		patches.find({
			$or: codeset
		}).sort({
			_id: 1
		}).toArray(function (e, o) {
			callback(o);

		});
	} else if (codeset.length == 0 && typeset.length > 0) {
		patches.find({
			$or: typeset
		}).sort({
			_id: 1
		}).toArray(function (e, o) {
			callback(o);


		});
	} else if (codeset.length == 0 && typeset.length == 0) {
		patches.find().toArray(function (e, o) {
			callback(o);


		});
	} else {
		patches.find({
			$and: [
				{
					$or: codeset
				},
				{
					$or: typeset
				}
        ]
		}).sort({
			_id: 1
		}).toArray(function (e, o) {
			callback(o);
		});
	}


}





exports.delAllRecords = function (callback) {
	accounts.remove({}, callback); // reset accounts collection for testing //
}

exports.checkDuplicatedObject = function (myArray, property) {
	var listOfDups = [];

	for (var i = 0; i < myArray.length; i++) {
		var count = 0;


		for (var j = 0; j < myArray.length; j++) {
			if (myArray[i][property] === myArray[j][property]) {
				count++;
			}
		}
		console.log(count);
		if (count > 1) {
			if (listOfDups.indexOf(myArray[i][property]) == -1) {
				listOfDups.push(myArray[i][property]);
			}

		}

	}
	return listOfDups;
}

/* private encryption & validation methods */
var generateSalt = function () {
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}
var md5 = function (str) {
	return crypto.createHash('md5').update(str).digest('hex');
}
var saltAndHash = function (pass, callback) {
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}
var validatePassword = function (plainPass, hashedPass, callback) {
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
	callback(null, hashedPass === validHash);
}
var getObjectId = function (id) {
	return new require('mongodb').ObjectID(id);
}
var findById = function (id, callback) {
	accounts.findOne({
		_id: getObjectId(id)
	}, function (e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
}
var findByMultipleFields = function (a, callback) {
	// this takes an array of name/val pairs to search against {fieldName : 'value'} //
	accounts.find({
		$or: a
	}).toArray(function (e, results) {
		if (e) callback(e)
		else callback(null, results)
	});
}
